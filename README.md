Polarion tools
--------------

Collection of tools for updating data in Polarion:

## polarion_docstrings

Project: https://gitlab.com/mkourim/polarion_docstrings

Test-related docstrings contain Polarion metadata like

```python

   """
   File description.

   Polarion:
       assignee: some_name
       caseimportance: medium
       casecomponent: vmaas
   """

   class TestClass:
       """Class description.

       Polarion:
           caseimportance: high
           testtype: functional
       """

       def test_this():
           """Test description.

           Polarion:
               assignee: some_other_user
               testtype: integration
           """
```

The `polarion_docstrings` library contain functionality for parsing this metadata and for validation of the metadata (linter).


## pytest-polarion-collect

Project: https://gitlab.com/mkourim/pytest-polarion-collect

Pytest plugin for collecting data from the metadata records related to test function. Uses the `polarion_docstrings` library for parsing metadata and produces JSON file with data about all collected tests.

## pytest-report-parameters

Project: https://gitlab.com/mkourim/pytest-report-parameters

Pytest plugin that extends JUnit XML report with information about test parameters so this information can be used in Polarion for the test case parametrization / iterations.

## polarion-tools-common

Project: https://gitlab.com/mkourim/polarion-tools-common

Library of common functionality shared by Polarion related tooling.

## dump2polarion

Project: https://github.com/mkoura/dump2polarion

Library for importing data in various formats (like in the JUnit XML produced by `pytest-report-parameters`) into Polarion using the Polarion importers services.

## cfme-testcases

Project: https://gitlab.com/mkourim/cfme-testcases

Tool / Library for importing test cases and test runs into Polarion. Glues together the tools mentioned above.
